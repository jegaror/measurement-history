import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const HistoryItem = props => {
    return (
        <View style={styles.item}>
            <Text style={styles.title}>{props.hour}</Text>
            <Text style={styles.title}>{props.data.heartRate}ppm</Text>
            <View style={props.data.hasAnomaly ? styles.circleAnomaly : styles.circleNoAnomaly}></View>
        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: "#eee",
        padding: 20,
        marginVertical: 1,
    },
    title: {
        flex: 1,
        fontSize: 16
    },
    circleNoAnomaly: {
        backgroundColor: '#01E4C7',
        marginTop: 5,
        width: 15,
        height: 15,
        borderRadius: 15/2
    },
    circleAnomaly: {
        backgroundColor: '#cc4598',
        marginTop: 5,
        width: 15,
        height: 15,
        borderRadius: 15/2
    }
});

export default HistoryItem;