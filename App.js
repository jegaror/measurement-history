import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Button,
  SectionList
} from "react-native";
import HistoryItem from "./components/HistoryItem";

import Constants from "expo-constants";
import moment from "moment";
import 'moment/locale/es'


export default App = () => {

  const API_KEY = "";
  const URL = `https://rithmi-frontend-test.s3-eu-west-1.amazonaws.com/samples.json${API_KEY}`;
  const [preFormatData, setPreFormatData] = useState([]);
  const [loading, setLoading ] = useState(true);

  const loadData = () => {
    console.log('refresh');
    setLoading(true);
    fetch(URL)
      .then((res) => {
        return res.json();
      }).then((resJson) => {
        setPreFormatData(resJson);
      }).catch( error => {
        console.error(error);
      }).finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  let localeData = preFormatData.map((item) => {
    item.key = moment(item.date).locale('es').format('YYYY-MM-DD');
    return item;
  }); // Obtain datetime

  localeData.sort((a,b) => {
    return moment(b.date).unix() - moment(a.date).unix()
  }); //Sort data by date

  const groupedData = localeData.reduce((accumulator, currentValue, currentIndex, array, key = currentValue.key) => {
    const keyObjectPosition = accumulator.findIndex((item) => item.key == key)
    if (keyObjectPosition >= 0) {
      accumulator[keyObjectPosition].data.push(currentValue)
      return accumulator
    } else {
      return accumulator.concat({ data: [currentValue], key: key })
    }
  }, []);
  console.log('render');
  return (
    <SafeAreaView 
      style={styles.container}
    >
      <View style={styles.centerButton}>
        <Button onPress={loadData} color="#01E4C7" title="Actualizar" />
      </View>
      {
      loading ? 
      <Text>Loading...</Text> 
      :
      <SectionList
        sections={groupedData}
        keyExtractor={(item, index) => item.date+index}
        initialNumToRender={8}
        renderItem={({ item }) => <HistoryItem data={item} hour={moment(item.date).format('HH:mm')}/>}
        renderSectionHeader={({ section: { key } }) => (
          <Text style={styles.header}>{moment(key).locale('es').format('dddd DD MMMM')}</Text>
        )}
      />
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    marginHorizontal: 16
  },
  header: {
    fontSize: 18,
    fontWeight: "bold",
    backgroundColor: "#fff",
    textTransform: 'capitalize',
    marginVertical: 10,
  },
  centerButton: {
    justifyContent: 'space-between',
  }
});
